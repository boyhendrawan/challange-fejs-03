import React from "react";
import BtnOrder from "./ContainerBtnOrder";
import Data from "./Data";
import BtnDelete from "./ContainerBtnDelete"

const ContainerData = (props) => {
  const userSearch = props.searchVal.toUpperCase();
  const [orderVal,setOrderVal]=React.useState("all");
  const dataSearch=props.allDatas.filter((e) => e.task.toUpperCase().includes(userSearch));
//  hanlder OrderBy
  let dataOrder;
  // handle order by show data from children
  const handlerCheckOrder =e => setOrderVal(e);
  // check which order used and dataOrder has function to holding orderby
  if(orderVal==="all")dataOrder=dataSearch;
  // just select has done base on search user
  else if(orderVal==="done") dataOrder=dataSearch.filter((e) => e.complete ===true);
  else dataOrder=dataSearch.filter((e) => e.complete ===false);


  return (
    <div className="mt-2">
      <BtnOrder onCheckApp={handlerCheckOrder} />
      <BtnDelete onDeleteAllCon={props.onDeleteAllApp} allDatas={props.allDatas}/>
      <div className="flex gap-y-4 flex-col">
        {
          //   handle with && Search For falsy
          dataOrder.length === 0 && <p className="text-center mt-4 text-lg font-semibold">Opps Data Not Available </p>
        }
        {dataOrder.length > 0 &&
          dataOrder.map((e) => <Data onEditDataCon={props.onEditDataApp} onContainerData={props.onUpdateCheckBox} onContainerDelete={props.onAppDelete} task={e.task} complete={e.complete} key={e.id} id={e.id}/>
          )}
      </div>
    </div>
  );
};

export default ContainerData;
