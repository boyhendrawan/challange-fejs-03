const TitleSearch = (e) => {
  return (
    <div className="justify-self-start">
      <h1 className="text-left text-4xl text-heading font-semibold tracking-wide ">
        Todolist
      </h1>
      <p className="pl-2 mt-4 text-xs font-light text-slate-500">
        Greeting From <span className="tracking-widest uppercase ">Hesoyam</span>
      </p>
      <p className="pl-2 mt-1 text-xs font-light text-slate-500">
        I Hope your day run as schedule to get better day
      </p>
    </div>
  );
};

export default TitleSearch;
