import React from "react";
import ListData from "./components/ListUser/ContainerData";
import ContainerForm from "./components/Search/ContainerForm";
import Card from "./UI/Card";
const dataBases=[{
  "id": 1,
  "task": "Nyuci mobil",
  "complete": true
}, {
  "id": 2,
  "task": "Memberi makan kucing",
  "complete": true
}, {
  "id": 3,
  "task": "Olahraga 10 menit",
  "complete": false
}, {
  "id": 4,
  "task": "Sarapan sereal",
  "complete": true
}, {
  "id": 5,
  "task": "Belanja harian",
  "complete": false
}, {
  "id": 6,
  "task": "Ngeprint tugas",
  "complete": true
}, {
  "id": 7,
  "task": "Bayar tagihan bulanan",
  "complete": true
}, {
  "id": 8,
  "task": "Berangkat kuliah",
  "complete": false
}, {
  "id": 9,
  "task": "Les bahasa Inggris",
  "complete": true
}, {
  "id": 10,
  "task": "Ke rumah Sabrina",
  "complete": false
}];

function App() {
  const [currentSearch,setSearch]=React.useState("");
  const [currentDatabase,setDatabase]=React.useState(dataBases);
  const handlerChildCon =e =>{
    //getDatafrom children
    setSearch(e);
  }
  const hanlderCheckBoxData=dataUpdate=>{
    // read current Data and updated it
    setDatabase(currentData=>{
      return currentData.map( e=> {
        // update data
        if(e.id ===dataUpdate.id) e.complete=dataUpdate.complete;
        return e;
      });
    })
  }
  // handler new input
  const hanlderNewData=e=>{
    const convertData={
      id:currentDatabase.length+1,
      task:e,
      complete:false,
    };
    // updated new data
    setDatabase(current=>[convertData,...current]);
  }
  // delete a data
 const handlerDeleteData=e=>{
  setDatabase(current=>current.filter(data=>data.id!==e));
  }
  const handlerUpdateData=e=>{
    setDatabase(current =>{
      return current.map(data=>{
        if(data.id===e.id) data.task=e.task;
        return data;
      });
    });
  }
  const handlerDeleteAll=statusDelete=>{
    if(statusDelete ==="all"){
      setDatabase([]);
    }
    else{
      setDatabase(prev=>{
        return prev.filter(e=> !e.complete);
      })
    }
  }
  return (
    <Card className="container mx-auto max-w-xl font-mainFont  md:my-4 shadow-inner w-[100%] ">
      <div className="w-full md:rounded-md shadow-sm shadow-black flex flex-col px-6 py-4">
        <ContainerForm onSearchApp={handlerChildCon} onNewdDataApp={hanlderNewData}/>
        <ListData onDeleteAllApp={handlerDeleteAll} onEditDataApp={handlerUpdateData}  onAppDelete={handlerDeleteData} onUpdateCheckBox={hanlderCheckBoxData} allDatas={currentDatabase} searchVal={currentSearch}/>
      </div>
    </Card>
  );
}

export default App;
