/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      keyframes: {
        wiggle: {
          '0%':{transform:'scale(1)'},
          '50%': { transform: 'scale(0)' },
          '100%': { display:"hidden" },
        }
      },
      animation: {
        change: 'wiggle 350ms ease-in-out ',
      },
      fontFamily:{
        "mainFont":["Montserrat"]
      },
      colors:{
        "main":"#eebbc3",
        "heading":"#232946",
        "pragraf":"#b8c1ec",
        "btn":"#67568c",
        "highlight":"#ff6e6c",
        "card":"#463366",
        "paragraf":"#c9bae2"
      }
    },
    
  },
  plugins: [],
}
